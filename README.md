## Layout builder copy/paste section

This module allows you to copy selected layout builder sections and paste them
within the same page or on a different page. Content and configuration is
duplicated so new instances are created for the content.


## Installation

  Install as you would normally install a contributed Drupal module. For further
  information, see
  [Installing Drupal Modules] (https://www.drupal.org/docs/extending-drupal/installing-modules)


## Configuration

There is no required configuration, only a permission that you need to enable
for roles you wish to grant this functionality.
